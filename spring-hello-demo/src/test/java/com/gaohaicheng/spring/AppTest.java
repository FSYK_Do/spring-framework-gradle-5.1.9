package com.gaohaicheng.spring;

import com.gaohaicheng.spring.po.Person;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * @author: ChenHao
 * @Description:
 * @Date: Created in 10:36 2019/6/19
 * @Modified by:
 */
public class AppTest {
    @Test
    public void MyTestBeanTest() {
        BeanFactory bf = new XmlBeanFactory( new ClassPathResource("spring-config.xml"));
        MyTestBean myTestBean = (MyTestBean) bf.getBean("myTestBean");
        System.out.println(myTestBean.getName());
		//AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    }

	@Test
    public void annotationConfigApplicationContext(){
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Cap3MainConfig.class);

		Person person = (Person)context.getBean("person");
		System.out.println(person);
	}
}